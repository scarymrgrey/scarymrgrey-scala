package wtf.scala.e01

object FizzBuzz {

  /**
    * Return "fizz", "buzz", "fizzbuzz" or number between 0 (inclusive) and n (exclusive).
    * For a given natural number greater than zero return:
    * "fizzbuzz" if the number is dividable by 3 and 5
    * "buzz" if the number is dividable by 5
    * "fizz" if the number is dividable by 3
    * the same number if no other requirement is fulfilled
    *
    * @param n
    * @throws IllegalArgumentException if n < 1
    * @return seq of fizzbuzz strings
    */
  @throws[IllegalArgumentException]
  private[e01] def fizzBuzzUntil(n: Int): Seq[String] = {
    require(n > 0)

    (0 until n).map(x => x match {
      case _ if (x % 15 == 0) => "fizzbuzz"
      case _ if (x % 3 == 0) => "fizz"
      case _ if (x % 5 == 0) => "buzz"
      case _ => x.toString
    })
  }

  def main(args: Array[String]): Unit = {
    println(fizzBuzzUntil(100).mkString(" "))
  }
}
